The bisimulation_algorithm.py tries to reduce the size (number of states and transitions) of a given model based on bisimilarity in respect to the given property.

example usage:
python bisimulation_algorithm.py herman17 "P=? [F \"stable\"]"
python bisimulation_algorithm.py brp-16-5 "P=? [F s=5 & srep=1 & i>8]"

The python script assumes that all the required files (.sta, .tra and .lab) have the same name and the appropriate ending.
In the first example, herman17.sta, herman17.tra and herman17.lab will be read in the current directory.
You can also specify an absolute or relative path to the files, like "prism/examples/herman17".
For the bisimulation, only the formula within the [] brackets is relevant, everything outside will be ignored.
The quotation marks for labels will need to be escaped.

New files generated are also .sta, .tra and .lab, using the same name as the original files with an added "-new"