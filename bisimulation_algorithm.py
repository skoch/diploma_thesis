from time import time
from decimal import Decimal
import argparse
import re

__author__ = "Stefan Koch"
"""
This script reads Prism's plain text style state, transition and label files and then calculates bisimulation
equivalence classes for the given transition system and a given condition. Accepts MDPs, CTMCs and DTMCs, output is
always a DTMC.
"""

parser = argparse.ArgumentParser(description='Reads a model from the appropriate Prism plain text files '
                                             '(.sta .tra and .lab), then tries to create a smaller DTMC model, '
                                             'bisimilar in regards to the given condition. '
                                             'The new model is saved as plain text Prism files in the same directory '
                                             'as the original model\'s files with "-new" added to their names.')
parser.add_argument('files', type=str, help='Absolute or relative path and name (without suffix) '
                                            'of the model files to be read')
parser.add_argument('condition', type=str, help='Prism Property to be used as a basis to reduce the model.'
                                                'Example: "P=? [ F x>=7 & y!=4]')
parser.add_argument('--type', type=str, help='Type of the model to be read, accepts "mdp", "ctmc" and "dtmc"(default)',
                    default='dtmc')
args = parser.parse_args()
files = args.files
cond = args.condition
model_type = args.type
states = []


def parse_condition(condition):
    """
    This function transforms the given Prism property into a condition readable by Python. This is done recursively.
    :param condition: (remaining part of the) Prism property given in the command line argument
    :return: A string that can be evaluated using Python's eval(), if U (Until) is used in the property, the resulting
             string needs to be split into several conditions
    """
    if len(condition) < 1:
        return condition
    if condition[0] in ['"']:
        for l in labels:
            if re.match(l, condition[1:]) is not None:
                return '"labels" in state and "' + label_dict[l] + '" in state["labels"]' + \
                       parse_condition(condition[len(l) + 2:])
    for v in variables:
        if re.match(v, condition) is not None:
            return 'state["var"]["' + v + '"]' + parse_condition(condition[len(v):])
    if condition[0:4] in ['true']:
        return 'True' + parse_condition(condition[4:])
    elif condition[0:5] in ['false']:
        return 'False' + parse_condition(condition[5:])
    elif len(condition) >= 2 and condition[0:2] in ['<=', '>=', '!=']:
        return condition[0:2] + parse_condition(condition[2:])
    elif condition[0] in ['F'] and condition[1] not in [' ']:
        return parse_condition(condition[condition.index(' '):])
    elif condition[0] in [' ', 'F', 'G', 'A', 'E']:
        return parse_condition(condition[1:])
    elif condition[0] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '<', '>', '(',  ')', '*', '+', '-']:
        return condition[0] + parse_condition(condition[1:])
    elif condition[0] in ['U']:
        return ' U ' + parse_condition(condition[condition.index(' '):])
    elif condition[0] in ['|']:
        return ' or ' + parse_condition(condition[1:])
    elif condition[0] in ['&']:
        return ' and ' + parse_condition(condition[1:])
    elif condition[0] in ['!']:
        return 'not ' + parse_condition(condition[1:])
    elif condition[0] in ['=']:
        return '==' + parse_condition(condition[1:])


def refine_groups(state_list, group_list):
    """
    This function creates a list summing up the probabilities for each state to transition to a group.
    Then new groups are derived based on that list.

    :param state_list: list of states
    :param group_list: list of groups from last iteration or initialization
    :return new_groups: list of lists of state-indices
    :return group_transitions: list (length = #states) of dictionaries {group_id: probability(float)} used to determine
                               equivalent states to assign new groups and (after the last iteration) to determine
                               transitions between equivalence classes
    """

    # initialize transition probability list of lists
    group_transitions = [[Decimal(0) for _ in group_list] for _ in state_list]
    # for each state: add up probabilities to transition to each group based on its successors
    for s_1 in state_list:
        for s_2 in s_1['succ'].keys():
            s_2_class = state_list[int(s_2)]['class']
            group_transitions[int(s_1['index'])][s_2_class] += Decimal(s_1['succ'][s_2])
    new_groups = []
    # search transition probability list for states with equal current group and transition probabilities
    i_1 = 0
    for p_1 in group_transitions:
        done = False
        # check whether the state is already assigned to a new group -> if yes, skip to next state
        for g in new_groups:
            if i_1 in g:
                done = True
        if not done:
            # state is not in any of the groups found beforehand so a new group is created with its index
            new_groups.append([i_1])
            i_2 = 0
            for p_2 in group_transitions:
                c_1 = state_list[i_1]['class']
                c_2 = state_list[i_2]['class']
                # i_1 < i_2: states with lower index and the state itself don't need to be checked
                # if current class and transition probabilities are equal for both states,
                #  they are similar up to this step and belong into the same group
                if i_1 < i_2 and p_1 == p_2 and c_1 == c_2:
                    new_groups[-1].append(i_2)
                i_2 += 1
        i_1 += 1
    return new_groups, group_transitions

t_start = time()  # start time

# read state file, creating a list of state dictionaries
# states = [{'index': state's index (integer),
#            'var': {'variable name': value (integer or True/False if boolean) for each variable},
#            'succ': {} (currently empty dictionary of successors and their transition probabilities)}
#           for each state]
with open(files + '.sta') as state_file:
    variables = state_file.readline()[1:-2].split(',')
    for line in state_file:
        [index, values] = line.split(':')
        values = values[1:-2].split(',')
        assert len(variables) == len(values), 'Error reading state File'
        new_state = {'index': int(index), 'var': {}, 'succ': {}}
        for i in range(len(variables)):
            if values[i] in ['true']:
                new_state['var'][variables[i]] = True
            elif values[i] in ['false']:
                new_state['var'][variables[i]] = False
            else:
                new_state['var'][variables[i]] = int(values[i])
        states.append(new_state)
print(len(states), 'states read successfully. T =', time() - t_start)

# read transition file, adding the second state as a successor to the appropriate sub-dictionary of the first
# 'succ': {'successor index': transition probability (float)}
with open(files + '.tra') as transition_file:
    [state_number, transition_number] = transition_file.readline().split(' ')
    if model_type in ['mdp']:
        for line in transition_file:
            [s1, distribution_id, s2, p] = line.split(' ')
            if s2 in states[int(s1)]['succ']:
                states[int(s1)]['succ'][s2] += Decimal(p)
            else:
                states[int(s1)]['succ'][s2] = Decimal(p)
    else:
        for line in transition_file:
            [s1, s2, p] = line.split(' ')
            states[int(s1)]['succ'][s2] = Decimal(p)
print('Transitions read successfully. T =', time() - t_start)


# if the given model is not a DTMC, transition probabilities are recalculated
# for MDPs this results in non-deterministic choices being treated as equally probable
# for CTMCs this derives the embedded DTMC
if model_type in ['mdp', 'ctmc']:
    for state in states:
        p_sum = Decimal(0.0)
        for successor in state['succ']:
            p_sum += Decimal(state['succ'][successor])
        for successor in state['succ']:
            state['succ'][successor] = Decimal(state['succ'][successor] / p_sum)

# read label file, adding the labels for each state as a list 'labels': []
with open(files + '.lab') as label_file:
    label_line = label_file.readline()
    for state_label_line in label_file:
        [state_id, state_labels] = state_label_line.split(': ')
        state_label_string = state_labels.split('\n')[0]
        states[int(state_id)]['labels'] = state_label_string.split(' ')
print('Labels read successfully. T =', time() - t_start)

# analyze the label file headline, creating a dictionary of labels
label_list = label_line.split('\n')[0].split(' ')
label_dict = {}
labels = []
for label_definition in label_list:
    [label_id, label_name] = label_definition.split('=')
    labels.append(eval(label_name))
    label_dict[eval(label_name)] = label_id

# to ensure correct parsing of variables and labels, they are sorted by their length, descending
variables.sort(key=lambda string: len(string), reverse=True)
labels.sort(key=lambda string: len(string), reverse=True)
parsed_cond = cond.split('[')[1].split(']')[0]
parsed_cond = parse_condition(parsed_cond)
parsed_cond = parsed_cond.split(' U ')

# separate states into groups depending on relevance using the given condition
groups = [[] for i in range(len(parsed_cond) + 1)]
for state in states:
    assigned = False
    i = len(parsed_cond) - 1
    while not assigned and i >= 0:
        if eval(parsed_cond[i]):
            groups[i + 1].append(state['index'])
            assigned = True
        i -= 1
    if not assigned:
        groups[0].append(state['index'])
i = 0
while len(groups) > i:
    if len(groups[i]) == 0:
        groups.remove(groups[i])
    else:
        i += 1
for i in range(len(groups)):
    for state in groups[i]:
        states[int(state)]['class'] = i
print('Initial groups assigned')

# iterates over groups using refine_groups until no new groups are created
new_length = len(groups)
group_tra = []
old_length = len(group_tra)
if len(groups) > 1:
    iteration = 0
    while old_length != new_length:
        iteration += 1
        print('Starting group refinement iteration', iteration)
        print('Number of groups:', new_length)
        print('T =', time() - t_start)
        old_length = len(groups)
        new_g, group_tra = refine_groups(states, groups)
        new_length = len(new_g)
        groups = new_g
        # assigns new class to all states based on new groups returned from function call
        for g_new in new_g:
            for s_index in g_new:
                states[s_index]['class'] = new_g.index(g_new)
else:
    group_tra = [[1.0]]
print('Equivalence classes determined. T =', time() - t_start)

# Creates a list of new states based on calculated equivalence classes using the values of the first state
# listed in each. Additionally, if one of the equivalent states was an initial state, the new state is an initial
# state in the new transition system
new_states = [{} for _ in range(len(groups))]
for group in groups:
    new_states[groups.index(group)] = states[group[0]]
    if 'labels' not in new_states[groups.index(group)]:
        new_states[groups.index(group)]['labels'] = []
    for state_id in group[1:]:
        if 'labels' in states[state_id]:
            if ('0' in states[state_id]['labels']) and ('0' not in new_states[groups.index(group)]['labels']):
                new_states[groups.index(group)]['labels'].append('0')

# creates a list of new transitions based on the latest group transition list generated in the refine_groups function
new_transitions = []
for state in new_states:
    state['succ'] = {}
    for x in range(len(group_tra[state['index']])):
        p = group_tra[state['index']][x]
        if p > 0:
            new_transitions.append({'s1': state['class'], 's2': x, 'p': p})
print('New states and transitions created. T =', time() - t_start)

# write new state file in Prism's plain text format
with open(files + '-new.sta', 'w') as new_state_file:
    var_string = '(' + variables[0]
    for var in variables[1:]:
        var_string += ',' + var
    var_string += ')\n'
    new_state_file.write(var_string)
    for state in new_states:
        state_line = str(state['class']) + ':(' + str(state['var'][variables[0]]).lower()
        for var in variables[1:]:
            state_line += ',' + str(state['var'][var]).lower()
        state_line += ')\n'
        new_state_file.write(state_line)

# write new label file in Prism's plain text format
with open(files + '-new.lab', 'w') as new_label_file:
    new_label_file.write(label_line)
    for state in new_states:
        if len(state['labels']) > 0:
            new_label_string = str(state['class']) + ': ' + str(state['labels'][0])
            for label in state['labels'][1:]:
                new_label_string += ' ' + str(label)
            new_label_string += '\n'
            new_label_file.write(new_label_string)

# write new transition file in Prism's plain text format
with open(files + '-new.tra', 'w') as new_trans_file:
    new_trans_file.write(str(len(new_states)) + ' ' + str(len(new_transitions)) + '\n')
    for transition in new_transitions:
        new_trans_file.write(str(transition['s1']) + ' ' + str(transition['s2']) +
                             ' ' + str(float(transition['p'])) + '\n')
print('New states and transitions successfully persisted. T =', time() - t_start)
